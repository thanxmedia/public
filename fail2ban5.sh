#!/bin/bash
# Script installs and configures Fail2Ban
 
# announce in syslog
logger "==================="
logger "Installing Fail2Ban"
logger "==================="
 
# enable EPEL repo for centos 5
wget http://download.fedoraproject.org/pub/epel/5/x86_64/epel-release-5-4.noarch.rpm
rpm -ivh epel-release-5-4.noarch.rpm
 
# enable EPEL repo for centos 6
#wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
#rpm -ivh epel-release-6-8.noarch.rpm
 
# install fail2ban
yum install -y fail2ban
 
# edit config file
MYHOSTNAME=`hostname`
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
sed -i 's/bantime  = 600/bantime  = 604800/g' /etc/fail2ban/jail.local
sed -i 's/ignoreip = 127.0.0.1\/8/ignoreip = 127.0.0.1\/8 216.133.219.2 216.133.218.130/g' /etc/fail2ban/jail.local
sed -i "s/name=SSH, dest=you@example\.com, sender=fail2ban@example\.com, sendername=\"Fail2Ban\"/name=SSH, dest=dleigh@thanxmedia\.com, sender=fail2ban@${MYHOSTNAME}\.com, sendername=\"Fail2Ban\"/g" /etc/fail2ban/jail.local
 
# start service
/sbin/chkconfig fail2ban on
service fail2ban start